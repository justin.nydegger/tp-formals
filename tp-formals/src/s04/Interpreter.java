package s04;

import java.util.HashMap;

public class Interpreter {
  private static Lexer lexer;
  // TODO - A COMPLETER (ex. 3) 
  
  public static int evaluate(String e) throws ExprException {
    lexer = new Lexer(e);
    int res = parseExpr();
    // test if nothing follows the expression...
    if (lexer.crtSymbol().length() > 0)
      throw new ExprException("bad suffix");
    return res;
  }

  private static int parseExpr() throws ExprException {
    int res = 0;
    // TODO - A COMPLETER
    return res;
  }

  private static int parseTerm() throws ExprException {
    int res = 0;
    // TODO - A COMPLETER
    return res;
  }

  private static int parseFact() throws ExprException {
     int res = 0;
    // TODO - A COMPLETER
    return res;
  }

  private static int applyFct(String fctName, int arg) throws ExprException {
    return 0; // TODO - A COMPLETER
  }
}
