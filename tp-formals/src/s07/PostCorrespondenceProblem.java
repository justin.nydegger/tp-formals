package s07;

public class PostCorrespondenceProblem {
  //==================================================
  static class Domino { 
    final String up, down; 
    Domino(String u, String d) {
      up=u; down=d;
    }
  }
  //==================================================
  static boolean solved(Domino[] t, int maxSteps) {
    return solved(t, maxSteps, "", "");
  }

  static boolean solved (Domino[] t, int k, String a, String b) {
    // TODO A COMPLETER...
    return false;
  }

  public static void main(String[] args) {
    Domino[] t1= {
        new Domino("100","1"), 
        new Domino("0","100"),
        new Domino("1","00"), 
    };
    Domino[] t2= {
        new Domino("10","0"), 
        new Domino("0","001"),
        new Domino("001","1"), 
    };
    
    Domino[] t3= {
        new Domino("1","111"), 
        new Domino("10111","10"),
        new Domino("10","0"), 
    };
    Domino[] t4= {
        new Domino("10","101"), 
        new Domino("011","11"),
        new Domino("101","011"), 
    };

    System.out.println(solved(t1, 50));
    System.out.println(solved(t2, 50));
    System.out.println(solved(t3, 50));
    System.out.println(solved(t4, 50));
  }

}
